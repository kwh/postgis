FROM splashblot/docker-postgis:latest

ADD . /postgis

RUN pip install -r /postgis/requirements.txt
RUN apt-get update
RUN apt-get install -y build-essential libssl-dev libffi-dev python-dev
RUN apt-get install -y libncurses5-dev bash s3cmd